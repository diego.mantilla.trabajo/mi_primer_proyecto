package com.example.misionticv19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

/*        Button btnBoton1 = (Button) findViewById(R.id.home_btnBoton1);
        ImageButton btnBoton2 = (ImageButton) findViewById(R.id.home_btnBoton2);
        FloatingActionButton btnBoton3 = (FloatingActionButton) findViewById(R.id.home_btnBoton3);
        ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.layout_home);

        btnBoton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verMensaje(
                        layout,
                        "Haz hecho click en el boton 1.",
                        Snackbar.LENGTH_SHORT );
            }
        });

        btnBoton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verMensaje(
                        layout,
                        "Haz hecho click en el boton 2",
                        Snackbar.LENGTH_LONG);
            }
        });

        btnBoton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verMensaje(
                        layout,
                        "Haz hecho click en el boton 3",
                        8000);
            }
        });*/
    }

    private void verMensaje(ConstraintLayout contexto, String msj, int duracion)
    {
/*        AlertDialog.Builder mensaje = new AlertDialog.Builder(contexto);
        mensaje.setTitle("Titulo");
        mensaje.setMessage(msj);
        mensaje.create();
        mensaje.show(); */

        Snackbar mensaje = Snackbar.make(contexto, msj, duracion);
        mensaje.show();
    }
}