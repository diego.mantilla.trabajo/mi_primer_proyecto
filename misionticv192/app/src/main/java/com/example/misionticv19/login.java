package com.example.misionticv19;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnAcceder = (Button) findViewById(R.id.login_btnAcceder);
        EditText txtEmail = (EditText) findViewById(R.id.login_txtEmail);
        EditText txtClave = (EditText) findViewById(R.id.login_txtClave);

        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtEmail.getText().toString().isEmpty() || txtClave.getText().toString().isEmpty()) {
                    AlertDialog.Builder mensaje = new AlertDialog.Builder(view.getContext());
                    mensaje.setTitle("Se ha producido un error");
                    mensaje.setMessage(R.string.login_btnAcceder_Mensaje);
                    mensaje.create();
                    mensaje.show();
                }
            }
        });
    }


}