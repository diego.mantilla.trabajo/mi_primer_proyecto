package com.example.misionticv19;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class relative extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative);

        /*EditText txwN1 = (EditText) findViewById(R.id.relative_txtN1);
        EditText txwN2 = (EditText) findViewById(R.id.relative_txtN2);*/
        Button btnSumar = (Button) findViewById(R.id.relative_btnSumar);
        TextView txwResultado = (TextView) findViewById(R.id.relative_resultado);
        ImageButton btnPuerta = (ImageButton) findViewById(R.id.relative_btnPuerta);
        ProgressBar pr = (ProgressBar) findViewById(R.id.progressBar);
        Switch btnSwitch = (Switch) findViewById(R.id.relative_switch);
        ToggleButton btnTogle = (ToggleButton) findViewById(R.id.relative_toogleButton);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //txwResultado.setText(btnSwitch.isChecked()? "Esta activado" : "No esta activado");
                txwResultado.setText(btnTogle.isChecked()? "Esta activado" : "No esta activado" );


                /*suma(txwN1, txwN2, txwResultado);
                int numero = Integer.parseInt(txwResultado.getText().toString());
                if(numero<=100)
                    pr.setProgress(numero);*/
            }
        });

        btnPuerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                suma(txwN1, txwN2, txwResultado);
                int numero = Integer.parseInt(txwResultado.getText().toString());
                if(numero<=100)
                    pr.setProgress(numero);*/
            }
        });

    }

    private void suma(EditText num1, EditText num2, TextView respuesta)
    {
        try {
            String numero1 = num1.getText().toString();
            String numero2 = num2.getText().toString();
            respuesta.setText(String.valueOf(Integer.parseInt(numero1) + Integer.parseInt(numero2)));
        }
        catch (Exception ex)
        {

        }
    }
}